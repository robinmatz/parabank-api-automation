# The service database resets itself after a certain time.
# Running the test cases will then fail, since the test user is no longer registered.
# Registration cannot be performed via api.
# 
# This script will perform the registration process.
# Requirements for this script are listed in 'requirements.txt'.

import getopt
import sys
import traceback

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def main(argv):
    driver = None
    is_remote = False
    try:

        opts, args = getopt.getopt(argv, "r", ["remote"])
        for opt, arg in opts:
            if opt in ("-r", "--remote"):
                is_remote = True

        if is_remote == False:
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--disable-dev-shm-usage')
            driver = webdriver.Chrome(options=chrome_options)
        if is_remote == True:
            driver = webdriver.Remote(
                command_executor="http://127.0.0.1:4444/wd/hub",
                desired_capabilities=DesiredCapabilities.CHROME
            )

        # Open homepage
        driver.get("https://parabank.parasoft.com/parabank/")
        # Navigate to registration page
        driver.find_element_by_xpath("//a[contains(text(),'Register')]").click()
        # Fill out form data
        driver.find_element_by_id("customer.firstName").send_keys("Robert")
        driver.find_element_by_id("customer.lastName").send_keys("Schiller")
        driver.find_element_by_id("customer.address.street").send_keys("Sample Street 1")
        driver.find_element_by_id("customer.address.city").send_keys("Sample Town")
        driver.find_element_by_id("customer.address.state").send_keys("Oklahoma")
        driver.find_element_by_id("customer.address.zipCode").send_keys("12345")
        driver.find_element_by_id("customer.phoneNumber").send_keys("0123456789")
        driver.find_element_by_id("customer.ssn").send_keys("111222333")
        driver.find_element_by_id("customer.username").send_keys("rschiller")
        driver.find_element_by_id("customer.password").send_keys("#Co@T3a3uNWB0GW")
        driver.find_element_by_id("repeatedPassword").send_keys("#Co@T3a3uNWB0GW")
        driver.find_element_by_xpath("//input[@value='Register']").click()

        # Either customer already exists
        try:
            WebDriverWait(driver, 5).until(
                EC.visibility_of_element_located((By.ID, "customer.username.errors")))
            print("Customer already exists - OK")
        # Or new account is created
        except:
            WebDriverWait(driver, 5).until(
                EC.visibility_of_element_located(
                    (By.XPATH, "//p[contains(text(),'Your account was created successfully.')]")))
            print("New customer created - OK")

    except Exception:
        traceback.print_exc(file=open("python_error.log","a"))

    finally:
        if driver is not None:
            driver.quit()

if __name__=="__main__":
    main(sys.argv[1:])
